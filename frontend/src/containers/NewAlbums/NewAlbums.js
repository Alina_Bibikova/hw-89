import React, {Component, Fragment} from 'react';
import {createAlbum} from "../../store/actions/actionsAlbums";
import {connect} from "react-redux";
import AlbumsForm from "../../components/AlbumsForm/AlbumsForm";
import {fetchArtist} from "../../store/actions/actionsArtist";

class NewAlbums extends Component {
    componentDidMount() {
        this.props.onFetchArtist();
    }

    createAlbum = albumData => {
        this.props.onAlbumCreated(albumData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h2>New album</h2>
                <AlbumsForm
                    onSubmit={this.createAlbum}
                    artist={this.props.artist}
                />
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    artist: state.artist.artist,
});

const mapDispatchToProps = dispatch => ({
    onAlbumCreated: albumData => dispatch(createAlbum(albumData)),
    onFetchArtist: () => dispatch(fetchArtist())
});

export default connect(mapStateToProps, mapDispatchToProps)(NewAlbums);