import React, {Component, Fragment} from 'react';
import {createPublishArtist, deleteArtist, fetchArtist} from "../../store/actions/actionsArtist";
import {connect} from "react-redux";
import ArtistListItems from "../../components/ArtistListItems/ArtistListItems";
import {Link} from "react-router-dom";
import {Button} from "reactstrap";

class Artist extends Component {
     componentDidMount() {
        this.props.onFetchArtist();
    }

    render() {
        return (
            <Fragment>
                <h2>
                    Artist
                    {this.props.user &&
                    <Link to="/new/artist">
                        <Button
                            color="primary"
                            className="float-right"
                        >
                            Add Artist
                        </Button>
                    </Link>
                    }
                </h2>
                {this.props.artist.map(artist => (
                    <ArtistListItems
                        key={artist._id}
                        _id={artist._id}
                        name={artist.name}
                        image={artist.image}
                        delete={() => this.props.deleteArtist(artist._id)}
                        publish={() => this.props.createPublishArtist(artist._id, true)}
                        user={this.props.user}
                        status={artist.published}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    artist: state.artist.artist,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    onFetchArtist: () => dispatch(fetchArtist()),
    deleteArtist: id => dispatch(deleteArtist(id)),
    createPublishArtist: (id, data) => dispatch(createPublishArtist(id, data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Artist);