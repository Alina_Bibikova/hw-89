import React, {Component} from 'react';
import {fetchTrackHistory} from "../../store/actions/actionsAlbums";
import {connect} from "react-redux";
import TrackHistoryListItems from "../../components/TrackHistoryListItems/TrackHistoryListItems";

class TrackHistory extends Component {
    componentDidMount() {
        this.props.onfetchTrackHistory();
    }

    render() {
        return (
            this.props.trackHistory ? <div>
                <h2>Track history</h2>
                {this.props.trackHistory.map(track => (
                        <TrackHistoryListItems
                            key={track._id}
                            nameTrack={track.track.nameTrack}
                            name={track.track.nameAlbum.nameArtist.name}
                            date={track.datetime}
                        />
                    ))}
            </div> : null
        );
    }
}

const mapStateToProps = state => ({
    trackHistory: state.albums.trackHistory,
});

const mapDispatchToProps = dispatch => ({
    onfetchTrackHistory: () => dispatch(fetchTrackHistory())
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackHistory);