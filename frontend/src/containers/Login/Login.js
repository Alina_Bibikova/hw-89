import React, {Component, Fragment} from 'react';
import {Alert, Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {connect} from "react-redux";
import {LoginUser} from "../../store/actions/usersActions";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

class Login extends Component {
    state = {
        username: '',
        password: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value});
    };

    submitFromHandler = event => {
        event.preventDefault();

        this.props.loginUser({...this.state})
    };

    render() {
        return (
            <Fragment>
                <h2>Login</h2>
                {this.props.error && (
                    <Alert color="danger">
                        {this.props.error.error || this.props.error.message || this.props.error.global}
                    </Alert>
                )}
                <Form onSubmit={this.submitFromHandler}>
                    <FormGroup>
                        <FacebookLogin/>
                    </FormGroup>

                    <FormElement
                        propertyName="username"
                        title="Username"
                        placeholder="Enter username your registered with"
                        value={this.state.username}
                        onChange={this.inputChangeHandler}
                        type="text"
                        autoComplete="current-username"
                    />

                    <FormElement
                        propertyName="password"
                        title="Password"
                        placeholder="Enter password"
                        value={this.state.password}
                        onChange={this.inputChangeHandler}
                        type="password"
                        autoComplete="current-password"
                    />

                    <FormGroup row>
                        <Col sm={{offset:2, size: 10}}>
                            <Button
                                type="submit"
                                color="primary"
                            >
                                Login
                            </Button>

                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
   error: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
    loginUser: userData => dispatch(LoginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);