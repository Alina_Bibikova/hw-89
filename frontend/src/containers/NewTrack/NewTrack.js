import React, {Component, Fragment} from 'react';
import TrackForm from "../../components/TrackForm/TrackForm";
import {createTrack, fetchAlbums} from "../../store/actions/actionsAlbums";
import {connect} from "react-redux";

class NewTrack extends Component {
    componentDidMount() {
        this.props.onFetchAlbums();
    }

    createTrack = trackData => {
        this.props.onTrackCreated(trackData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h2>New track</h2>
                <TrackForm
                    onSubmit={this.createTrack}
                    fetchAlbums={this.props.fetchAlbums}
                />
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    fetchAlbums: state.albums.fetchAlbums,
});

const mapDispatchToProps = dispatch => ({
    onTrackCreated: trackData => dispatch(createTrack(trackData)),
    onFetchAlbums: () => dispatch(fetchAlbums())
});

export default connect(mapStateToProps, mapDispatchToProps)(NewTrack);