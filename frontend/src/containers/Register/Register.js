import React, {Component, Fragment} from 'react';
import {Alert, Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {registerUser} from "../../store/actions/usersActions";
import FormElement from "../../components/UI/Form/FormElement";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

class Register extends Component {
    state = {
        username: '',
        displayName: '',
        password: '',
        avatar: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value});
    };

    submitFromHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.registerUser(formData)
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    fieldHasError = fieldName => {
        return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message
    };

    render() {
        return (
            <Fragment>
                <h2>Register new user</h2>
                {this.props.error && this.props.error.global && (
                    <Alert color="danger">
                        {this.props.error.global}
                    </Alert>
                )}
                <Form onSubmit={this.submitFromHandler}>
                    <FormGroup>
                        <FacebookLogin/>
                    </FormGroup>

                    <FormElement
                    propertyName="username"
                    title="Username"
                    placeholder="Enter your desired username"
                    value={this.state.username}
                    onChange={this.inputChangeHandler}
                    error={this.fieldHasError('username')}
                    type="text"
                    autoComplete="new-username"
                    />

                    <FormElement
                        propertyName="displayName"
                        title="Display name"
                        placeholder="Enter display name"
                        value={this.state.displayName}
                        onChange={this.inputChangeHandler}
                        error={this.fieldHasError('displayName')}
                        type="displayName"
                        autoComplete="new-displayName"
                    />

                    <FormElement
                        propertyName="password"
                        title="Password"
                        placeholder="Enter new secure password"
                        value={this.state.password}
                        onChange={this.inputChangeHandler}
                        error={this.fieldHasError('password')}
                        type="password"
                        autoComplete="new-password"
                    />

                    <FormGroup row>
                        <Label sm={2} for="avatar">Image</Label>
                        <Col sm={10}>
                            <Input
                                type="file"
                                name="avatar" id="avatar"
                                onChange={this.fileChangeHandler}
                            />
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Col sm={{offset:2, size: 10}}>
                            <Button
                                type="submit"
                                color="primary"
                            >
                                Register
                            </Button>

                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
    registerUser: UserData => dispatch(registerUser(UserData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);