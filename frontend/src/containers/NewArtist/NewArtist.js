import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {createArtist} from "../../store/actions/actionsArtist";
import ArtistForm from "../../components/ArtistForm/ArtistForm";

class NewArtist extends Component {
    createArtist = artistData => {
        this.props.onArtistCreated(artistData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h2>New artist</h2>
                <ArtistForm
                    onSubmit={this.createArtist}
                />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onArtistCreated: artistData => dispatch(createArtist(artistData)),
});

export default connect(null, mapDispatchToProps)(NewArtist);