import React from 'react';
import PropTypes from  'prop-types';

import {Button, Card, CardBody, CardFooter} from "reactstrap";
import MusicThumbnail from "../MusicThumbnail/MusicThumbnail";
import {Link} from "react-router-dom";

const ArtistListItems = props => {
    return (
        <Card key={props._id} style={{marginTop: '10px'}}>
            <CardBody>
                <MusicThumbnail image={props.image}/>
                <Link to={'/infoAlbums/' + props._id}>
                    {props.name}
                </Link>
            </CardBody>

            <CardFooter>
                {props.user && props.user.role === 'admin' ? <Button style={{marginRight: '10px'}} color='danger' onClick={props.delete}>Delete</Button> : null}

                {props.user && props.user.role === 'admin' && props.status === false ? <Button style={{marginRight: '10px'}} color='info' onClick={props.publish}>Publish</Button> : null}
            </CardFooter>
        </Card>
    );
};

ArtistListItems.propTypes = {
    image: PropTypes.string,
    _id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
};

export default ArtistListItems;