import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import AlbumsListItems from "../AlbumsListItems/AlbumsListItems";
import {createPublishAlbum, deleteAlbums, infoAlbums} from "../../store/actions/actionsArtist";
import {Link} from "react-router-dom";
import {Button} from "reactstrap";

class InfoAlbums extends Component {
    componentDidMount() {
        this.props.infoAlbums(this.props.match.params.id)
    }

    render() {
        return (
            this.props.infoAlbum ? <Fragment>
                <h2>Albums
                    {this.props.user &&
                    <Link to="/new/album">
                        <Button
                            color="primary"
                            className="float-right"
                        >
                            Add Album
                        </Button>
                    </Link>
                    }
                </h2>
                {this.props.infoAlbum.map(album => {
                    return (
                        <AlbumsListItems
                            key={album._id}
                            _id={album._id}
                            image={album.image}
                            description={album.description}
                            album={album.nameAlbums}
                            date={album.date}
                            name={album.nameArtist.name}
                            delete={() => this.props.deleteAlbums({album: album._id, artist: this.props.match.params.id})}
                            publish={() => this.props.createPublishAlbum({album: album._id, artist: this.props.match.params.id}, true)}
                            user={this.props.user}
                            status={album.published}
                        />
                    )
                })}
            </Fragment> : null

        );
    }
}

const mapStateToProps = state => ({
    infoAlbum: state.albums.albums,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    infoAlbums: id => dispatch(infoAlbums(id)),
    deleteAlbums: id => dispatch(deleteAlbums(id)),
    createPublishAlbum: (id, data) => dispatch(createPublishAlbum(id, data))
});

export default connect(mapStateToProps, mapDispatchToProps)(InfoAlbums);