import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";

class TrackForm extends Component {
    state = {
        nameTrack: '',
        nameAlbum: '',
        longest: '',
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.onSubmit(this.state);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>

                <FormGroup row>
                    <Label sm={2} for="nameTrack">Name track</Label>
                    <Col sm={10}>
                        <Input
                            type="text" required
                            name="nameTrack" id="nameTrack"
                            placeholder="Enter name track"
                            value={this.state.nameTrack}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label sm={2} for="nameAlbum">Name album</Label>
                    <Col sm={10}>
                        <Input
                            type="select" required
                            name="nameAlbum" id="nameAlbum"
                            value={this.state.nameAlbum}
                            onChange={this.inputChangeHandler}
                        >
                            {this.props.fetchAlbums.map(album => (
                                <option key={album._id} value={album._id}>{album.nameAlbums}</option>
                            ))}
                        </Input>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label sm={2} for="longest">Longest</Label>
                    <Col sm={10}>
                        <Input
                            type="number" required
                            name="longest" id="longest"
                            placeholder="Enter longest"
                            value={this.state.longest}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col sm={{offset:2, size: 10}}>
                        <Button type="submit" color="primary">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default TrackForm;