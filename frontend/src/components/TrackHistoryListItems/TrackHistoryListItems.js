import React from 'react';
import {Card, CardBody, CardText} from "reactstrap";
import PropTypes from "prop-types";

const TrackHistoryListItems = props => {
    return (
        <Card key={props._id} style={{marginTop: '10px'}}>
            <CardBody>
                Name track: {props.nameTrack}
                <CardText>Name artist: {props.name}</CardText>
                <CardText>Date: {props.date}</CardText>
            </CardBody>
        </Card>
    );
};

TrackHistoryListItems.propTypes = {
    nameTrack: PropTypes.string,
    name: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired,
};

export default TrackHistoryListItems;