import React, {Component} from 'react';
import {connect} from "react-redux";
import {createPublishTrack, deleteTrack, infoTrack} from "../../store/actions/actionsAlbums";
import TracksListItems from "../TracksListItems/TracksListItems";
import {Link} from "react-router-dom";
import {Button} from "reactstrap";

class InfoTracks extends Component {
    componentDidMount() {
        this.props.infoTrack(this.props.match.params.id)
    }

    render() {
        return (
            this.props.infoTracks ? <div>
                <h2>Tracks
                    {this.props.user &&
                    <Link to="/new/track">
                        <Button
                            color="primary"
                            className="float-right"
                        >
                            Add Track
                        </Button>
                    </Link>
                    }
                </h2>
                {this.props.infoTracks.map(tracks => (
                    <TracksListItems
                         key={tracks._id}
                         trackNumber={tracks.trackNumber}
                         nameTrack={tracks.nameTrack}
                         longest={tracks.longest}
                         delete={() => this.props.deleteTrack({track: tracks._id, album: this.props.match.params.id})}
                         publish={() => this.props.createPublishTrack({track: tracks._id, album: this.props.match.params.id}, true)}
                         user={this.props.user}
                         status={tracks.published}
                    />
                ))}
            </div> : null
        );
    }
}

const mapStateToProps = state => ({
    infoTracks: state.albums.infoTracks,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    infoTrack: id => dispatch(infoTrack(id)),
    deleteTrack: id => dispatch(deleteTrack(id)),
    createPublishTrack: (id, data) => dispatch(createPublishTrack(id, data))
});

export default connect(mapStateToProps, mapDispatchToProps)(InfoTracks);