import React from 'react';
import {Button, Card, CardBody, CardFooter} from "reactstrap";
import PropTypes from "prop-types";

const TracksListItems = props => {
    return (
        <Card onClick={props.click} key={props._id} style={{marginTop: '10px'}}>
            <CardBody>
                <p>Number: {props.trackNumber}</p>
                <p>Track: {props.nameTrack}</p>
                <p>longest: {props.longest}</p>
            </CardBody>
            <CardFooter>
                {props.user && props.user.role === 'admin' ? <Button style={{marginRight: '10px'}} color='danger' onClick={props.delete}>Delete</Button> : null}

                {(props.user && props.user.role === 'admin') && props.status === false ? <Button style={{marginRight: '10px'}} color='info' onClick={props.publish}>Publish</Button> : null}
            </CardFooter>
        </Card>
    );
};

TracksListItems.propTypes = {
    nameTrack: PropTypes.string,
    trackNumber: PropTypes.string,
    longest: PropTypes.string,
};

export default TracksListItems;
