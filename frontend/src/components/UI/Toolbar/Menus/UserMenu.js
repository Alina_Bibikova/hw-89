import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, NavItem, NavLink, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';
import AvatarThumbnail from "../../../AvatarThumbnail/AvatarThumbnail";

const UserMenu = ({user, logout}) => (

    <UncontrolledDropdown nav inNavbar>
        <DropdownToggle nav caret>
            <AvatarThumbnail user={user} avatar={user.avatar}/>
            Hello, {user.displayName}
        </DropdownToggle>
        <DropdownMenu right>
            <DropdownItem>
                Show profile
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/tracks" exact>Tracks</NavLink>
                </NavItem>
            </DropdownItem>
            <DropdownItem>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/track_history" exact>Track history</NavLink>
                </NavItem>
            </DropdownItem>
            <DropdownItem onClick={logout}>
                Logout
            </DropdownItem>
        </DropdownMenu>
    </UncontrolledDropdown>
);

export default UserMenu;