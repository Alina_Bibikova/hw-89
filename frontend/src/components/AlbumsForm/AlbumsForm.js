import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";

class AlbumsForm extends Component {
    state = {
        nameAlbums: '',
        nameArtist: '',
        date: '',
        image: '',
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>

                <FormGroup row>
                    <Label sm={2} for="nameAlbum">Name Album</Label>
                    <Col sm={10}>
                        <Input
                            type="text" required
                            name="nameAlbums" id="nameAlbums"
                            placeholder="Enter name album"
                            value={this.state.nameAlbums}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label sm={2} for="nameArtist">Name Artist</Label>
                    <Col sm={10}>
                        <Input
                            type="select" required
                            name="nameArtist" id="nameArtist"
                            value={this.state.nameArtist}
                            onChange={this.inputChangeHandler}
                        >
                            {this.props.artist.map(artist => (
                            <option key={artist._id} value={artist._id}>{artist.name}</option>
                            ))}
                         </Input>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label sm={2} for="date">Year of release of an album</Label>
                    <Col sm={10}>
                        <Input
                            type="number" required
                            name="date" id="date"
                            placeholder="Enter date"
                            value={this.state.date}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label sm={2} for="image">Image</Label>
                    <Col sm={10}>
                        <Input
                            type="file"
                            name="image" id="image"
                            onChange={this.fileChangeHandler}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col sm={{offset:2, size: 10}}>
                        <Button type="submit" color="primary">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default AlbumsForm;