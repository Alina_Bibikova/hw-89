import axios from '../../axios-api';
import {push} from "connected-react-router";
import {NotificationManager} from "react-notifications";

export const INFO_TRACKS_SUCCESS = 'INFO_TRACKS_SUCCESS';
export const FETCH_TRACKS_FAILURE = 'FETCH_TRACKS_FAILURE';
export const FETCH_TRACK_SUCCESS = 'FETCH_TRACK_SUCCESS';
export const FETCH_TRACK_FAILURE = 'FETCH_TRACK_FAILURE';
export const FETCH_TRACK_HISTORY_SUCCESS = 'FETCH_TRACK_HISTORY_SUCCESS';
export const FETCH_TRACK_HISTORY_FAILURE = 'FETCH_TRACK_HISTORY_FAILURE';
export const CREATE_TRACK_HISTORY_SUCCESS = 'CREATE_TRACK_HISTORY_SUCCESS';
export const CREATE_TRACK_HISTORY_FAILURE = 'CREATE_TRACK_HISTORY_FAILURE';

export const CREATE_TRACK_SUCCESS = 'CREATE_TRACK_SUCCESS';
export const CREATE_TRACK_FAILURE = 'CREATE_TRACK_FAILURE';
export const DELETE_TRACK_FAILURE = 'DELETE_TRACK_FAILURE';

export const CREATE_ALBUM_SUCCESS = 'CREATE_ALBUM_SUCCESS';
export const CREATE_ALBUM_FAILURE = 'CREATE_ALBUM_FAILURE';

export const FETCH_ALBUM_SUCCESS = 'FETCH_ALBUM_SUCCESS';
export const FETCH_ALBUM_FAILURE = 'FETCH_ALBUM_FAILURE';

export const CREATE_PUBLISH_TRACK_SUCCESS = 'CREATE_PUBLISH_TRACK_SUCCESS';
export const CREATE_PUBLISH_TRACK_FAILURE = 'CREATE_PUBLISH_TRACK_FAILURE';

export const fetchTrackSuccess = tracks => ({type: FETCH_TRACK_SUCCESS, tracks});
export const fetchTrackFailure = error => ({type: FETCH_TRACK_FAILURE, error});
export const infoTracksSuccess = data => ({type: INFO_TRACKS_SUCCESS, data});
export const fetchTracksFailure = error => ({type: FETCH_TRACKS_FAILURE, error});
export const fetchTrackHistorySuccess = trackHistory => ({type: FETCH_TRACK_HISTORY_SUCCESS, trackHistory});
export const fetchTrackHistoryFailure = error => ({type: FETCH_TRACK_HISTORY_FAILURE, error});
export const createTrackHistorySuccess = () => ({type: CREATE_TRACK_HISTORY_SUCCESS});
export const createTrackHistoryFailure = error => ({type: CREATE_TRACK_HISTORY_FAILURE, error});

export const createTrackSuccess = () => ({type: CREATE_TRACK_SUCCESS});
export const createTrackFailure = error => ({type: CREATE_TRACK_FAILURE, error});
export const deleteTrackFailure = error => ({type: DELETE_TRACK_FAILURE, error});

export const createAlbumSuccess = () => ({type: CREATE_ALBUM_SUCCESS});
export const createAlbumsFailure = error => ({type: CREATE_ALBUM_FAILURE, error});

export const fetchAlbumsSuccess = fetchAlbums => ({type: FETCH_ALBUM_SUCCESS, fetchAlbums});
export const fetchAlbumsFailure = error => ({type: FETCH_ALBUM_FAILURE, error});

export const createPublishTrackSuccess = () => ({type: CREATE_PUBLISH_TRACK_SUCCESS});
export const createPublishTrackFailure = error => ({type: CREATE_PUBLISH_TRACK_FAILURE, error});

export const fetchAlbums = () => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        if(!user) {
            dispatch(push('/login'));
        } else {
            const config = {headers: {'Authorization': user.token}};

            return axios.get('/albums', config).then(
                response => dispatch(fetchAlbumsSuccess(response.data))
            ).catch(error => dispatch(fetchAlbumsFailure(error)))
        }
    };
};

export const fetchTrackHistory = () => {
    return (dispatch, getState) => {
        const token = getState().users.user;
        if (!token) {
            dispatch(push('/login'))
        } else {
            return axios.get('/track_history', {headers: {'Authorization': token.token}}).then(
                response => dispatch(fetchTrackHistorySuccess(response.data))
            ).catch(error => dispatch(fetchTrackHistoryFailure(error)));
        }
    };
};

export const createTrackHistory = TrackHistoryData => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        if (!token) {
            dispatch(push('/login'));
        } else {
            return axios.post('/track_history', {track: TrackHistoryData}, {headers: {'Authorization': token}}).then(
                () => dispatch(createTrackHistorySuccess())
            ).catch(error => dispatch(createTrackHistoryFailure(error)));
        }
    };
};

export const infoTrack = id => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        if(!user) {
            dispatch(push('/login'));
        } else {
            const config = {headers: {'Authorization': user.token}};

            axios.get(`/tracks?albums=${id}`, config).then(
                response => dispatch(infoTracksSuccess(response.data))
            ).catch(error => dispatch(fetchTracksFailure(error)));
        }
    };
};

export const fetchTracks = () => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        if (!user) {
            dispatch(push('/login'));
        } else {
            const config = {headers: {'Authorization': user.token}};

            return axios.get('/tracks', config).then(
                response => dispatch(fetchTrackSuccess(response.data))
            ).catch(error => dispatch(fetchTrackFailure(error)));
        }
    };
};

export const createTrack = trackData => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const config = {headers: {'Authorization': token}};

        return axios.post('/tracks', trackData, config).then(
            () => dispatch(createTrackSuccess())
        ).catch(error => dispatch(createTrackFailure(error)));
    };
};

export const createAlbum = albumData => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const config = {headers: {'Authorization': token}};

        return axios.post('/albums', albumData, config).then(
            () => dispatch(createAlbumSuccess())
        ).catch(error => dispatch(createAlbumsFailure(error)));
    };
};

export const deleteTrack = data => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        const config = {headers: {'Authorization': token}};

        try {
            await axios.delete(`/tracks/${data.track}`, config);
            NotificationManager.success('Delete track!');
            dispatch(infoTrack(data.album));
        } catch (error) {
            dispatch(deleteTrackFailure(error));
        }
    };
};

export const createPublishTrack = (id, data) => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const config = {headers: {'Authorization': token}};

        return axios.post(`/tracks/publish/${id.track}`, {status: data}, config).then(
            () => dispatch(createPublishTrackSuccess())
        ).then(() =>  dispatch(infoTrack(id.album))
        ).catch(error => dispatch(createPublishTrackFailure(error)));
    };
};

