import axios from '../../axios-api';
import {push} from 'connected-react-router';
import {NotificationManager} from 'react-notifications';

export const FETCH_ARTIST_SUCCESS = 'FETCH_ARTIST_SUCCESS';
export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const FETCH_ARTIST_FAILURE = 'FETCH_ARTIST_FAILURE';
export const FETCH_ALBUMS_FAILURE = 'FETCH_ALBUMS_FAILURE';
export const CREATE_ARTIST_SUCCESS = 'CREATE_ARTIST_SUCCESS';
export const DELETE_ARTIST_FAILURE = 'DELETE_ARTIST_FAILURE';

export const CREATE_PUBLISH_SUCCESS = 'CREATE_PUBLISH_SUCCESS';
export const CREATE_PUBLISH_ALBUMS_SUCCESS = 'CREATE_PUBLISH_ALBUMS_SUCCESS';
export const CREATE_PUBLISH_ARTIST_FAILURE = 'CREATE_PUBLISH_ARTIST_FAILURE';
export const DELETE_ALBUM_FAILURE = 'DELETE_ALBUM_FAILURE';
export const DELETE_PUBLISH_ALBUM_FAILURE = 'DELETE_PUBLISH_ALBUM_FAILURE';

export const fetchArtistSuccess = artist => ({type: FETCH_ARTIST_SUCCESS, artist});
export const infoAlbumsSuccess = data => ({type: FETCH_ALBUMS_SUCCESS, data});
export const fetchArtistFailure = error => ({type: FETCH_ARTIST_FAILURE, error});
export const fetchAlbumsFailure = error => ({type: FETCH_ALBUMS_FAILURE, error});
export const createArtistSuccess = () => ({type: CREATE_ARTIST_SUCCESS});
export const deleteArtistFailure = error => ({type: DELETE_ARTIST_FAILURE, error});
export const deleteAlbumFailure = error => ({type: DELETE_ALBUM_FAILURE, error});

export const createPublishArtistSuccess = () => ({type: CREATE_PUBLISH_SUCCESS});
export const createPublishArtistFailure = error => ({type: CREATE_PUBLISH_ARTIST_FAILURE, error});

export const createPublishAlbumsSuccess = () => ({type: CREATE_PUBLISH_ALBUMS_SUCCESS});
export const deletePublishAlbumFailure = error => ({type: DELETE_PUBLISH_ALBUM_FAILURE, error});

export const fetchArtist = () => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        if(!user) {
            dispatch(push('/login'));
        } else {
            const config = {headers: {'Authorization': user.token}};

            return axios.get('/artist', config).then(
                response => dispatch(fetchArtistSuccess(response.data))
            ).catch(error => dispatch(fetchArtistFailure(error)));
        }
    };
};

export const infoAlbums = id => {
    return (dispatch, getState) => {
        const user = getState().users.user;
        const config = {headers: {'Authorization': user.token}};

        return axios.get(`/albums?artist=${id}`, config).then(
            response => dispatch(infoAlbumsSuccess(response.data))
        ).catch(error => dispatch(fetchAlbumsFailure(error)));
    };
};


export const createArtist = artistData => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const config = {headers: {'Authorization': token}};

        return axios.post('/artist', artistData, config).then(
            () => dispatch(createArtistSuccess())
        ).catch(error => dispatch(fetchAlbumsFailure(error)));
    };
};

export const deleteArtist = id => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        const config = {headers: {'Authorization': token}};

        try {
            await axios.delete(`/artist/${id}`, config);
            NotificationManager.success('Delete artist!');
            await dispatch(fetchArtist());
        } catch (error) {
            dispatch(deleteArtistFailure(error))
        }
    };
};

export const createPublishArtist = (id, data) => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const config = {headers: {'Authorization': token}};

        return axios.post(`/artist/publish/${id}`, {status: data}, config).then(
            () => dispatch(createPublishArtistSuccess())
        ).then(() =>  dispatch(fetchArtist())
        ).catch(error => dispatch(createPublishArtistFailure(error)));
    };
};

export const deleteAlbums = id => {
    return async (dispatch, getState) => {
        const token = getState().users.user.token;
        const config = {headers: {'Authorization': token}};

        try {
            await axios.delete(`/albums/${id.album}`, config);
            NotificationManager.success('Delete album!');
            await dispatch(infoAlbums(id.artist));
        } catch (error) {
            dispatch(deleteAlbumFailure(error));
        }
    };
};

export const createPublishAlbum = (id, data) => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const config = {headers: {'Authorization': token}};

        return axios.post(`/albums/publish/${id.album}`, {status: data}, config).then(
            () => dispatch(createPublishAlbumsSuccess())
        ).then(() =>  dispatch(infoAlbums(id.artist))
        ).catch(error => dispatch(deletePublishAlbumFailure(error)));
    };
};
