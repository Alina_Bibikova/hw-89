import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Route, Switch, withRouter} from "react-router";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import Artist from "./containers/Artist/Artist";
import InfoAlbums from "./components/InfoAlbums/InfoAlbums";
import InfoTracks from "./components/InfoTracks/InfoTracks";
import {connect} from "react-redux";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Tracks from "./containers/Tracks/Tracks";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import NewArtist from "./containers/NewArtist/NewArtist";
import {logoutUser} from "./store/actions/usersActions";
import NewAlbums from "./containers/NewAlbums/NewAlbums";
import NewTrack from "./containers/NewTrack/NewTrack";
import {NotificationContainer} from "react-notifications";

class App extends Component {
  render() {
      return (
        <Fragment>
            <NotificationContainer/>
            <header>
                <Toolbar user={this.props.user}
                         logout={this.props.logoutUser}/>
            </header>
            <Container style={{marginTop: '20px'}}>
                <Switch>
                    <Route path="/" exact component={Artist}/>
                    <Route path="/new/artist" exact component={NewArtist}/>
                    <Route path="/new/album" exact component={NewAlbums}/>
                    <Route path="/new/track" exact component={NewTrack}/>
                    <Route path="/infoAlbums/:id" exact component={InfoAlbums}/>
                    <Route path="/infoTrack/:id" exact component={InfoTracks}/>
                    <Route path="/register" exact component={Register} />
                    <Route path="/login" exact component={Login} />
                    <Route path="/tracks" exact component={Tracks} />
                    <Route path="/track_history" exact component={TrackHistory} />
                </Switch>
            </Container>
        </Fragment>
    );
  }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
