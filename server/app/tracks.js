const express = require('express');
const Tracks = require('../models/Track');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const router = express.Router();

router.get('/', auth, (req, res) => {
    if(req.user.role === 'user') {
        if (req.query.albums) {
            Tracks.find({nameAlbum: req.query.albums, published: true}).sort('trackNumber').populate({path: 'nameAlbum', populate: {path: 'nameArtist', model: 'Artist'}})
                .then(result => {res.send(result)})
                .catch(() => res.sendStatus(500));
        } else {
            Tracks.find({published: true}).populate({path: 'nameAlbum', populate: {path: 'nameArtist', model: 'Artist'}})
                .then(track => res.send(track))
                .catch(() => res.sendStatus(500));
        }
    } if (req.user.role === 'admin') {
        if (req.query.albums) {
            Tracks.find({nameAlbum: req.query.albums}).sort('trackNumber').populate({path: 'nameAlbum', populate: {path: 'nameArtist', model: 'Artist'}})
                .then(result => {res.send(result)})
                .catch(() => res.sendStatus(500));
        } else {
            Tracks.find().populate({path: 'nameAlbum', populate: {path: 'nameArtist', model: 'Artist'}})
                .then(track => res.send(track))
                .catch(() => res.sendStatus(500));
        }
    }
});

router.get('/:id', (req, res) => {
    Tracks.findById(req.params.id).populate({path: 'nameAlbum', populate: {path: 'nameArtist', model: 'Artist'}})
        .then(result => {
            if (result) return res.send(result);
            res.sendStatus(404)})
        .catch(() => res.sendStatus(500));
});

router.post('/', auth, async (req, res) => {
    try{
        const trackData = req.body;
        const track = new Tracks(trackData);
        const count = await Tracks.count({nameAlbum: trackData.nameAlbum});
        trackData.trackNumber = count + 1;
        const result = await track.save();
        res.send(result);
    } catch (error) {
        res.status(400).send(error)
    }
});

router.put('/:id', async (req, res) => {
    try {
        const updateTrack = await Tracks.findById(req.params.id);
        updateTrack.nameTrack = req.body.nameTrack;
        updateTrack.longest = req.body.longest;
        updateTrack.nameAlbum = req.body.nameAlbum;
        updateTrack.trackNumber = req.body.trackNumber;

        await updateTrack.save();
        return res.send(updateTrack);
    } catch (error) {
        return res.status(400).send(error)
    }
});

router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    try {
        await Tracks.deleteOne({_id: req.params.id});
        return res.sendStatus(200);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.post('/publish/:id', [auth, permit('admin')], async (req, res) => {
    try {
        const tracks = await Tracks.findById(req.params.id);
        tracks.published = req.body.status;
        tracks.save();
        res.send({message: "Published"})
    } catch (e) {
        res.send({message: "Error occured"})
    }
});

module.exports = router;