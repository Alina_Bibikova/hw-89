const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');
const Albums = require('../models/Album');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', auth, (req, res) => {
    if(req.user.role === 'user') {
        if(req.query.artist) {
            Albums.find({nameArtist: req.query.artist, published: true}).sort('date').populate('nameArtist')
                .then(result => {res.send(result)})
                .catch(() => res.sendStatus(500));
        } else {
            Albums.find({published: true}).sort('date').populate('nameArtist')
                .then(albums => {res.send(albums)})
                .catch(() => res.sendStatus(500));
        }
    } if(req.user.role === 'admin') {
        if (req.query.artist) {
            Albums.find({nameArtist: req.query.artist}).sort('date').populate('nameArtist')
                .then(result => {res.send(result)})
                .catch(() => res.sendStatus(500));
        } else {
            Albums.find().sort('date').populate('nameArtist')
                .then(result => {res.send(result)})
                .catch(() => res.sendStatus(500));
        }
    }
});

router.get('/:id', (req, res) => {
    Albums.findById(req.params.id).populate('nameArtist')
        .then(result => {
            if (result) return res.send(result);
            res.sendStatus(404);})
        .catch(() => res.sendStatus(500));
});

router.post('/', auth, upload.single('image'), (req, res) => {
    const AlbumData = req.body;
    if (req.file) {
        AlbumData.image = req.file.filename;
    }
    const albums = new Albums(AlbumData);

    albums.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});

router.put('/:id', upload.single('image'), async (req, res) => {
    try {
        const updateAlbum = await Albums.findById(req.params.id);
        updateAlbum.nameAlbums = req.body.nameAlbums;
        updateAlbum.nameArtist = req.body.nameArtist;
        updateAlbum.date = req.body.date;
        updateAlbum.image = req.body.image;

        if (req.file) {
            updateAlbum.image = req.file.filename;
        }
        await updateAlbum.save();
        return res.send(updateAlbum);
    } catch (error) {
        return res.status(400).send(error)
    }
});

router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    try {
        await Albums.deleteOne({_id: req.params.id});
        return res.sendStatus(200);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.post('/publish/:id', [auth, permit('admin')], async (req, res) => {
    try {
        const albums = await Albums.findById(req.params.id);
        albums.published = req.body.status;
        albums.save();
        res.send({message: "Published"})
    } catch (e) {
        res.send({message: "Error occured"})
    }
});

module.exports = router;