const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');
const auth = require('../middleware/auth');

const router = express.Router();

router.get('/', auth, (req, res) => {
    if (req.user) {
        TrackHistory.find({user: req.user._id}).sort('datetime').populate({
            path: 'track',
            populate: {path: 'nameAlbum', model: 'Album',
                populate: {
                    path: 'nameArtist',
                    model: 'Artist'
                }
            },

        })
            .then(result => res.send(result))
            .catch(() => res.sendStatus(500));
    } else {
        TrackHistory.find().populate({
            path: 'track',
            populate: {path: 'nameAlbum', model: 'Album',
                populate: {
                    path: 'nameArtist',
                    model: 'Artist'
                }
            },

        })
            .then(track => res.send(track))
            .catch(() => res.sendStatus(500));
    }
});

router.post('/', async (req, res) => {
    const token = req.get('Authorization');
    const user = await User.findOne({token: token});
    if (!user) {
        return res.status(401).send({error: 'Unauthorized'});
    }

    const trackHistory = new TrackHistory({
        user: user._id,
        track: req.body.track,
        datetime: new Date().toString('yyyy-MM-dd')
    });

        trackHistory.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});

module.exports = router;