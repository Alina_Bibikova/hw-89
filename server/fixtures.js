const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Albums = require('./models/Album');
const Tracks = require('./models/Track');
const User = require('./models/User');
const nanoid = require("nanoid");

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    await User.create({
        avatar: 'avatar.png',
        username: 'user',
        password: '123',
        displayName: 'Don Joe',
        role: 'user',
        token: nanoid()

    },

    {
        avatar: 'avatar.png',
        username: 'admin',
        password: '123',
        displayName: 'Jack Dan',
        role: 'admin',
        token: nanoid()
    });

    const [johny, adele, lana] = await Artist.create(
        {   name: 'Johnny Cash',
            description: 'Hurt',
            image: 'johnnyCash.jpg',
            published: true

        },
        {
            name: 'Adele',
            description: 'Someone Like You',
            image: 'adel.jpeg',
            published: false
        },
        {
            name: 'Lana Del Rey',
            description: 'Ride',
            image: 'lana.jpg',
            published: true
        },
    );

    const [album1, album2, album3, album4, album5] =  await Albums.create(
        {   nameAlbums: 'The Man Comes Around',
            nameArtist: johny._id,
            date: 2002,
            image: 'cash.jpg',
            published: true,
        },
        {   nameAlbums: 'The Man Comes Around',
            nameArtist: johny._id,
            date: 2007,
            image: 'cash.jpg',
            published: false,
        },
        {   nameAlbums: 'The Man Comes Around',
            nameArtist: johny._id,
            date: 2004,
            image: 'cash.jpg',
            published: false,
        },
        {   nameAlbums: 'Adele 21',
            nameArtist: adele._id,
            date: 2011,
            image: 'adele.jpg',
            published: true
        },

        {   nameAlbums: 'Born to Die',
            nameArtist: lana._id,
            date: 2012,
            image: 'born.jpg',
            published: true
        },
    );

    await Tracks.create(
        {   nameTrack: 'Hurt',
            nameAlbum: album1._id,
            longest: '04:30',
            trackNumber: 1,
            published: true
        },
        {   nameTrack: 'Hurt',
            nameAlbum: album1._id,
            longest: '04:30',
            trackNumber: 3,
            published: true
        },
        {   nameTrack: 'Hurt',
            nameAlbum: album1._id,
            longest: '04:30',
            trackNumber: 2,
            published: true
        },
        {   nameTrack: 'Hurt',
            nameAlbum: album1._id,
            longest: '04:30',
            trackNumber: 5,
            published: false

        },
        {   nameTrack: 'Hurt',
            nameAlbum: album1._id,
            longest: '04:30',
            trackNumber: 4,
            published: false
        },


        {   nameTrack: 'Hurt',
            nameAlbum: album2._id,
            longest: '04:30',
            trackNumber: 2,
            published: true
        },
        {   nameTrack: 'Hurt',
            nameAlbum: album2._id,
            longest: '04:30',
            trackNumber: 5,
            published: false

        },
        {   nameTrack: 'Hurt',
            nameAlbum: album2._id,
            longest: '04:30',
            trackNumber: 4,
            published: false
        },

        {   nameTrack: 'Hurt',
            nameAlbum: album3._id,
            longest: '04:30',
            trackNumber: 2,
            published: true
        },
        {   nameTrack: 'Hurt',
            nameAlbum: album3._id,
            longest: '04:30',
            trackNumber: 5,
            published: false

        },
        {   nameTrack: 'Hurt',
            nameAlbum: album3._id,
            longest: '04:30',
            trackNumber: 4,
            published: false
        },

        {   nameTrack: 'Someone Like You',
            nameAlbum: album4._id,
            longest: '03:30',
            trackNumber: 1,
            published: true
        },

        {   nameTrack: 'Someone Like You',
            nameAlbum: album4._id,
            longest: '03:30',
            trackNumber: 3,
            published: false
        },

        {   nameTrack: 'Someone Like You',
            nameAlbum: album4._id,
            longest: '03:30',
            trackNumber: 2,
            published: true
        },
        {   nameTrack: 'Someone Like You',
            nameAlbum: album4._id,
            longest: '03:30',
            trackNumber: 4,
            published: false
        },

        {   nameTrack: 'Born to Die',
            nameAlbum: album5._id,
            longest: '03:30',
            trackNumber: 1,
            published: true
        },

        {   nameTrack: 'Born to Die',
            nameAlbum: album5._id,
            longest: '03:30',
            trackNumber: 3,
            published: false

        },

        {   nameTrack: 'Born to Die',
            nameAlbum: album5._id,
            longest: '03:30',
            trackNumber: 2,
            published: true
        },
    );

    await connection.close();
};

run().catch(error => {
    console.error('Something went wrong', error);
});