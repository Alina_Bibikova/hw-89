const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TracksSchema = new Schema({
    nameTrack: {
        type: String, required: true
    },
    nameAlbum: {
        type: Schema.ObjectId,
        ref: 'Album',
    },
    published: {
        type: Boolean,
        default: false,
        enum: [true, false]
    },
    longest: String,
    trackNumber: String
});

const Track = mongoose.model('Track', TracksSchema);

module.exports = Track;