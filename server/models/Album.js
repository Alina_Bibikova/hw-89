const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AlbumsSchema = new Schema({
    nameAlbums: {
        type: String,
        required: true
    },
    nameArtist: {
        type: Schema.ObjectId,
        ref: 'Artist',
    },
    published: {
        type: Boolean,
        default: false,
        enum: [true, false]
    },
    date: String,
    image: String,
});

const Album = mongoose.model('Album', AlbumsSchema);

module.exports = Album;
